﻿using System;

namespace Lab1._2
{
    class TasksSolver
    {
        public double SolveTask1(double x, double y, double z)
        {
            double result = ((2 * Math.Cos(x - (Math.PI / 6))) / 0.5 * Math.Pow(Math.Sin(y), 2)) * (1 + (Math.Pow(z, 2) / (3 - (Math.Pow(z, 2) / 5))));
            return result;
        }

        public double SolveTask3(double x, double y, double z)
        {
            double result = (1 + Math.Pow(Math.Sin(x + y), 2)) / (Math.Abs(x - ((2 * y) / (1 + Math.Pow(x, 2) * Math.Pow(y, 2))))) * Math.Pow(x, Math.Abs(y)) + Math.Pow(Math.Cos(1 / z), 2);
            return result;
        }

        public double SolveTask4(double x, double y, double z)
        {
            double result = (Math.Pow(Math.Abs(Math.Cos(x) - Math.Cos(y)), (1 + 2 * Math.Pow(Math.Sin(y), 2)))) * (1 + z + Math.Pow(z, 2) / 2 + Math.Pow(z, 3) / 3 + Math.Pow(z, 4) / 4);
            return result;
        }

        public double SolveTask8(double x, double y, double z)
        {
            double result = Math.Pow(2, -1 * x) * Math.Sqrt((x + Math.Pow(Math.Abs(y), (double)1 / 4))) * Math.Pow((Math.Exp(x - 1 / Math.Sin(z))), (double)1 / 3);
            return result;
        }        
    }
}
