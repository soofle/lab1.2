﻿using System;
using System.Text;
using System.IO;

namespace Lab1._2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;

            string dataPath = "Input data.txt";
            if (!File.Exists(dataPath))
            {
                Console.WriteLine("Файл с исходными данными не найден!");
                return;
            }

            double[,] array = new ArrayCreator().CreateArrayFromFile(dataPath);

            Console.WriteLine("Входные параметры:\n");
            ShowArray(array);
            Console.WriteLine();

            ShowResults(array);
        }
        public static void ShowArray(double[,] array)
        {
            for (int i = 0; i < array.Length/3; i++)
            {
                for (int j = 0; j < 3; j++)
                    Console.Write(array[i, j] + " ");
                Console.WriteLine();
            }
        }
        public static void ShowResults(double[,] array)
        {
            var tasksSolver = new TasksSolver();
            for (int i = 0; i < array.Length/3; i++)
            {
                double x = array[i, 0], y = array[i, 1], z = array[i, 2];
                Console.WriteLine("Параметры:x={0} y={1} z={2}", x, y, z);
                Console.Write("Результат задачи №1: ");
                Console.WriteLine(tasksSolver.SolveTask1(x, y, z));
                Console.Write("Результат задачи №3: ");
                Console.WriteLine(tasksSolver.SolveTask3(x, y, z));
                Console.Write("Результат задачи №4: ");
                Console.WriteLine(tasksSolver.SolveTask4(x, y, z));
                Console.Write("Результат задачи №8: ");
                Console.WriteLine(tasksSolver.SolveTask8(x, y, z));
                Console.WriteLine();
            }
        }
    }
}