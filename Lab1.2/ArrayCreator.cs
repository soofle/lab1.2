﻿using System.IO;

namespace Lab1._2
{
    public class ArrayCreator
    {
        public double[,] CreateArrayFromFile(string dataPath)
        {

            string[] allLines = File.ReadAllLines(dataPath);

            int numberOfParametres = GetNumberOfParametres(allLines);
            var array = new double[numberOfParametres, 3];
            VariableSet[] inputDatas = new VariableSet[numberOfParametres];

            int count = 0;
            foreach (string line in allLines) //string || var?            
            {
                if (!string.IsNullOrWhiteSpace(line))
                    inputDatas[count++] = new VariableSet(line.Split());
            }

            for (int i = 0; i < numberOfParametres; i++)
            {
                array[i, 0] = inputDatas[i].X;
                array[i, 1] = inputDatas[i].Y;
                array[i, 2] = inputDatas[i].Z;
            }
            return array;
        }

        private int GetNumberOfParametres(string[] allLines)
        {
            int lineCounts = 0;
            foreach (string line in allLines)
            {
                if (!string.IsNullOrWhiteSpace(line))
                    lineCounts++;
            }
            return lineCounts;
        }

    }
}