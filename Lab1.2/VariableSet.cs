﻿using System;

namespace Lab1._2
{
    class VariableSet
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }
        public VariableSet() { }
        public VariableSet(string[] args)
        {
            try
            {
                X = double.Parse(args[0]);
                Y = double.Parse(args[1]);
                Z = double.Parse(args[2]);
            }
            catch(Exception e)
            {
                Console.WriteLine("Есть строка с неверными параметрами!");
                Console.WriteLine(e.Message);
                X = 0;Y = 0;Z = 0;
            }
        }

    }
}
